package edu.bbte.idde.nzim2062.backend.dao;

import edu.bbte.idde.nzim2062.backend.model.Counter;
import edu.bbte.idde.nzim2062.backend.model.Menu;

public interface MenuDao extends Dao<Menu> {
    Menu findByName(String name);

    Counter getCounter();
}
