package edu.bbte.idde.nzim2062.backend.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class Menu extends BaseEntity {
    private Long restaurant;
    private String name;
    private Double price;
    private String ingredients;
    private String possiblealergies;
    private Boolean vegan;

    public Menu() {
        super();
    }

    public Menu(Long id, String menuName, Double price, String ingredients,
                String possibleAlergies, Boolean vegan, Long restaurant) {
        this();
        this.id = id;
        this.name = menuName;
        this.price = price;
        this.ingredients = ingredients;
        this.possiblealergies = possibleAlergies;
        this.vegan = vegan;
        this.restaurant = restaurant;
    }

    public Menu(String menuName, Double price, String ingredients,
                String possibleAlergies, Boolean vegan, Long restaurant) {
        this();
        this.name = menuName;
        this.price = price;
        this.ingredients = ingredients;
        this.possiblealergies = possibleAlergies;
        this.vegan = vegan;
        this.restaurant = restaurant;
    }

    public Boolean allFieldsAreThere() {
        return this.getName() != null
                &&
                this.getIngredients() != null
                &&
                this.getPossiblealergies() != null
                &&
                this.getVegan() != null
                &&
                this.getPrice() != null
                &&
                this.getRestaurant() != null;
    }
}
