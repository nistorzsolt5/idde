package edu.bbte.idde.nzim2062.backend.model;

import lombok.Data;

@Data
public class Counter {
    private int read;
    private int write;
}
