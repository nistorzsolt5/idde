package edu.bbte.idde.nzim2062.backend.dao.jdbc;

import edu.bbte.idde.nzim2062.backend.dao.RestaurantDao;
import edu.bbte.idde.nzim2062.backend.model.Restaurant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

public class JdbcRestaurantDao implements RestaurantDao {
    private final DataSource dataSource;
    private static final Logger LOG = LoggerFactory.getLogger(JdbcRestaurantDao.class);

    public JdbcRestaurantDao() {
        dataSource = DataSourceFactory.getDataSource();
    }

    private Collection<Restaurant> resultSetCollection(ResultSet set) throws SQLException {
        Collection<Restaurant> restaurants = new ArrayList<>();
        while (set.next()) {
            restaurants.add(new Restaurant(
                    set.getLong("id"),
                    set.getString("name")
            ));
        }
        return restaurants;
    }

    private Restaurant resultSet(ResultSet set) throws SQLException {
        if (set.next()) {
            return new Restaurant(set.getLong("id"),
                    set.getString("name"));
        }
        return null;
    }

    @Override
    public void create(Restaurant restaurant) {
        LOG.info("Creating a new restaurant\n");

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("insert into Restaurants values(?, default)");
            prep.setString(1, restaurant.getName());
            prep.executeUpdate();
        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
        }
    }

    @Override
    public void update(Long id, Restaurant restaurant) {
        LOG.info("Updating restaurant: {} to: {}\n", this, restaurant);

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("""
                            update Restaurants
                            set name = ?,
                            where id = ?;""");
            prep.setString(1, restaurant.getName());
            prep.setLong(2, id);
            prep.executeUpdate();
        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
        }
    }

    @Override
    public void delete(Long id) {
        LOG.info("Removing a restaurant\n");

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("delete from Restaurants where id = ?;");
            prep.setLong(1, id);
            prep.executeUpdate();
        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
        }
    }

    @Override
    public Collection<Restaurant> findAll() {
        LOG.info("Finding all restaurants\n");

        Collection<Restaurant> restaurants = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("select * from Restaurants");
            ResultSet set = prep.executeQuery();
            restaurants = resultSetCollection(set);
        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
        }
        return restaurants;
    }

    @Override
    public Restaurant findById(Long id) {
        LOG.info("Finding a restaurant by the ID: {}\n", id);

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("select * from Restaurants where id = ?;");
            prep.setLong(1, id);
            ResultSet set = prep.executeQuery();
            return resultSet(set);
        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
        }
        return null;
    }

    @Override
    public Restaurant findByName(String name) {
        LOG.info("Finding a restaurant by name: {}\n", name);

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("select * from Restaurants where name = ?;");
            prep.setString(1, name);
            ResultSet set = prep.executeQuery();
            return resultSet(set);

        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
        }
        return null;
    }
}
