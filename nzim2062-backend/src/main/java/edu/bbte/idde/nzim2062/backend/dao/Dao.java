package edu.bbte.idde.nzim2062.backend.dao;

import edu.bbte.idde.nzim2062.backend.model.BaseEntity;

import java.util.Collection;

public interface Dao<T extends BaseEntity> {
    T findById(Long id);

    void create(T data);

    void update(Long id, T data);

    void delete(Long id);

    Collection<T> findAll();
}
