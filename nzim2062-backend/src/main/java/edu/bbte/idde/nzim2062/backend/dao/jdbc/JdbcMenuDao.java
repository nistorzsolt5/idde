package edu.bbte.idde.nzim2062.backend.dao.jdbc;


import edu.bbte.idde.nzim2062.backend.config.Config;
import edu.bbte.idde.nzim2062.backend.config.ConfigFactory;
import edu.bbte.idde.nzim2062.backend.dao.MenuDao;
import edu.bbte.idde.nzim2062.backend.model.Counter;
import edu.bbte.idde.nzim2062.backend.model.Menu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

public class JdbcMenuDao implements MenuDao {
    private final DataSource dataSource;

    private Config config = ConfigFactory.getConfig();

    private Counter counter = new Counter();
    private static final Logger LOG = LoggerFactory.getLogger(JdbcMenuDao.class);

    public JdbcMenuDao() {
        dataSource = DataSourceFactory.getDataSource();
    }

    private Collection<Menu> resultSetCollection(ResultSet set)
            throws SQLException {
        Collection<Menu> menus = new ArrayList<>();
        while (set.next()) {
            menus.add(new Menu(
                    set.getLong("id"),
                    set.getString("menuName"),
                    set.getDouble("price"),
                    set.getString("possibleAlergies"),
                    set.getString("ingredients"),
                    set.getBoolean("vegan"),
                    set.getLong("FK")
            ));
        }
        return menus;
    }

    private Menu resultSet(ResultSet set) throws SQLException {
        if (set.next()) {
            return new Menu(
                    set.getLong("id"),
                    set.getString("menuName"),
                    set.getDouble("price"),
                    set.getString("possibleAlergies"),
                    set.getString("ingredients"),
                    set.getBoolean("vegan"),
                    set.getLong("FK")
            );
        }
        return null;
    }

    private void prepareStatementMenu(Menu menu, PreparedStatement prep) throws SQLException {
        prep.setString(1, menu.getName());
        prep.setDouble(2, menu.getPrice());
        prep.setString(3, menu.getPossiblealergies());
        prep.setString(4, menu.getIngredients());
        prep.setBoolean(5, menu.getVegan());
        prep.setLong(6, menu.getRestaurant());
    }

    @Override
    public void create(Menu menu) {
        counter.setWrite(counter.getWrite() + 1);
        LOG.info("Creating a new menu update ; flag: {} ; counter: {}\n", config.isLogUpdates(), counter.getWrite());

        if (config.isLogUpdates()) {
            LOG.info("insert into Menus values(?, ?, ?, ?, ?, default, ?)");
        }

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("insert into Menus values(?, ?, ?, ?, ?, default, ?)");
            prepareStatementMenu(menu, prep);
            prep.executeUpdate();
        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
        }
    }

    @Override
    public void update(Long id, Menu menu) {
        counter.setWrite(counter.getWrite() + 1);
        LOG.info("Updating menu: {} to: {}; flag: {} ; counter: {}\n", findById(id), menu, config.isLogUpdates(), counter.getWrite());

        if (config.isLogUpdates()) {
            LOG.info("update Menus set menuName = ?, price = ?, possibleAlergies = ?, ingredients = ?, vegan = ?, FK = ? where id = ?;");
        }

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("""
                            update Menus
                            set menuName = ?,
                            price = ?,
                            possibleAlergies = ?,
                            ingredients = ?,
                            vegan = ?,
                            FK = ?
                            where id = ?;""");
            prepareStatementMenu(menu, prep);
            prep.setLong(7, id);
            prep.executeUpdate();
        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
        }
    }

    @Override
    public void delete(Long id) {
        counter.setWrite(counter.getWrite() + 1);
        LOG.info("Removing a menu; flag: {} ; counter: {}\n", config.isLogUpdates(), counter.getWrite());

        if (config.isLogUpdates()) {
            LOG.info("delete from Menus where id = ?");
        }

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("delete from Menus where id = ?;");
            prep.setLong(1, id);
            prep.executeUpdate();
        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
        }
    }

    @Override
    public Menu findById(Long id) {
        counter.setRead(counter.getRead() + 1);

        LOG.info("Finding a menu by the ID: {}; flag: {} ; counter: {}\n", id, config.isLogQueries(), counter.getRead());

        if (config.isLogQueries()) {
            LOG.info("select * from Menus where id = ?");
        }

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("select * from Menus where id = ?;");
            prep.setLong(1, id);
            ResultSet set = prep.executeQuery();
            return resultSet(set);

        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
        }
        return null;
    }

    @Override
    public Collection<Menu> findAll() {
        counter.setRead(counter.getRead() + 1);
        LOG.info("Finding all menus; flag: {} ; counter: {}\n", config.isLogQueries(), counter.getRead());

        if (config.isLogQueries()) {
            LOG.info("select * from Menus");
        }

        Collection<Menu> menus = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("select * from Menus");
            ResultSet set = prep.executeQuery();
            menus = resultSetCollection(set);

        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
        }
        return menus;
    }

    @Override
    public Menu findByName(String name) {
        counter.setRead(counter.getRead() + 1);
        LOG.info("Finding a menu by the ID; flag: {} ; counter: {}\n", config.isLogQueries(), counter.getRead());

        if (config.isLogQueries()) {
            LOG.info("select * from Menus where menuName = ?");
        }

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("select * from Menus where menuName = ?;");
            prep.setString(1, name);
            ResultSet set = prep.executeQuery();
            return resultSet(set);
        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
        }
        return null;
    }

    @Override
    public Counter getCounter() {
        return counter;
    }
}