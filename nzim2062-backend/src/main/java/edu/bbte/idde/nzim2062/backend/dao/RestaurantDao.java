package edu.bbte.idde.nzim2062.backend.dao;

import edu.bbte.idde.nzim2062.backend.model.Restaurant;

public interface RestaurantDao extends Dao<Restaurant> {
    Restaurant findByName(String name);
}
