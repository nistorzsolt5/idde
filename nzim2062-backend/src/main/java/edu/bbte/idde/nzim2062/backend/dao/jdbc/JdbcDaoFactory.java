package edu.bbte.idde.nzim2062.backend.dao.jdbc;

import edu.bbte.idde.nzim2062.backend.dao.DaoFactory;
import edu.bbte.idde.nzim2062.backend.dao.MenuDao;
import edu.bbte.idde.nzim2062.backend.dao.RestaurantDao;

public class JdbcDaoFactory extends DaoFactory {
    private static JdbcMenuDao menuDao;

    private static JdbcRestaurantDao restaurantDao;


    @Override
    public synchronized MenuDao getMenuDao() {
        if (menuDao == null) {
            menuDao = new JdbcMenuDao();
        }
        return menuDao;
    }

    @Override
    public synchronized RestaurantDao getRestaurantDao() {
        if (restaurantDao == null) {
            restaurantDao = new JdbcRestaurantDao();
        }
        return restaurantDao;
    }
}
