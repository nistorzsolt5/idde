package edu.bbte.idde.nzim2062.backend.dao.mem;

import edu.bbte.idde.nzim2062.backend.dao.DaoFactory;
import edu.bbte.idde.nzim2062.backend.dao.MenuDao;
import edu.bbte.idde.nzim2062.backend.dao.RestaurantDao;
import edu.bbte.idde.nzim2062.backend.model.Menu;
import edu.bbte.idde.nzim2062.backend.model.Restaurant;

public class MemDaoFactory extends DaoFactory {
    private static MenuDao dao;
    private static RestaurantDao restaurantDao;

    @Override
    public synchronized MenuDao getMenuDao() {
        if (dao == null) {
            dao = new MemMenuDao();
            dao.create(new Menu("Rantott hus", 13.0, "csirkemell, krumpli, fuszerek", "gluten", false, 1L));
            dao.create(new Menu("Ceasar salata", 10.0, "salata, mittomen meg mi", "", true, 1L));
            dao.create(new Menu("Krumpli salata", 11.0, "salata, krumpli, mittomen meg mi", "", true, 1L));
        }
        return dao;
    }

    @Override
    public synchronized RestaurantDao getRestaurantDao() {
        if (restaurantDao == null) {
            restaurantDao = new MemRestaurantDao();
            restaurantDao.create(new Restaurant("Szep csarda"));
            restaurantDao.create(new Restaurant("Vagi Gagyi Etterem"));
        }
        return restaurantDao;
    }
}