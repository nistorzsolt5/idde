package edu.bbte.idde.nzim2062.backend.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class Restaurant extends BaseEntity {
    protected String name;

    public Restaurant() {
        super();
    }

    public Restaurant(Long id, String name) {
        this();
        this.id = id;
        this.name = name;
    }

    public Restaurant(String name) {
        this();
        this.name = name;
    }
}
