package edu.bbte.idde.nzim2062.backend.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class BaseEntity implements Serializable {
    protected Long id;
}
