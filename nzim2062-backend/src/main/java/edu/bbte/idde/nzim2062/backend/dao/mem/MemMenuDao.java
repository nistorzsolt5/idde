package edu.bbte.idde.nzim2062.backend.dao.mem;

import edu.bbte.idde.nzim2062.backend.dao.MenuDao;
import edu.bbte.idde.nzim2062.backend.model.Counter;
import edu.bbte.idde.nzim2062.backend.model.Menu;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MemMenuDao implements MenuDao {
    private static final Map<Long, Menu> ENTITIES = new ConcurrentHashMap<>();

    private static final AtomicLong ID_GENERATOR = new AtomicLong();

    private static final Logger LOG = LoggerFactory.getLogger(MemMenuDao.class);

    @Override
    public Menu findById(Long id) {
        LOG.info("Finding a menu by the ID\n");
        return ENTITIES.get(id);
    }

    @Override
    public void create(Menu menu) {
        LOG.info("Creating a new menu\n");
        Long id = ID_GENERATOR.getAndIncrement();
        menu.setId(id);
        ENTITIES.put(id, menu);
    }

    @Override
    public void update(Long id, Menu menu) {
        LOG.info("Updating menu: {} to: {}\n", this, menu);
        ENTITIES.replace(id, menu);
    }

    @Override
    public void delete(Long id) {
        LOG.info("Removing a menu\n");
        ENTITIES.remove(id);
    }

    @Override
    public Collection<Menu> findAll() {
        LOG.info("Finding all menus\n");
        return ENTITIES.values();
    }

    @Override
    public Menu findByName(String name) {
        LOG.info("Finding a menu by the ID\n");

        for (Menu menu : ENTITIES.values()) {
            if (menu.getName().equals(name)) {
                return menu;
            }
        }
        return null;
    }

    @Override
    public Counter getCounter() {
        return new Counter();
    }

}
