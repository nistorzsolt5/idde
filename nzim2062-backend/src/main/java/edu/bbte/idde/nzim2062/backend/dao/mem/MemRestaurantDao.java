package edu.bbte.idde.nzim2062.backend.dao.mem;

import edu.bbte.idde.nzim2062.backend.dao.RestaurantDao;
import edu.bbte.idde.nzim2062.backend.model.Restaurant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class MemRestaurantDao implements RestaurantDao {
    private static final Logger LOG = LoggerFactory.getLogger(MemRestaurantDao.class);

    private static final AtomicLong ID_GENERATOR = new AtomicLong();
    private static final Map<Long, Restaurant> ENTITIES = new ConcurrentHashMap<>();

    @Override
    public Restaurant findById(Long id) {
        LOG.info("Finding a restaurant by the ID: {}\n", id);
        return ENTITIES.get(id);
    }

    @Override
    public void create(Restaurant restarant) {
        LOG.info("Creating a new restaurant\n");
        Long id = ID_GENERATOR.getAndIncrement();
        restarant.setId(id);
        ENTITIES.put(id, restarant);
    }

    @Override
    public void update(Long id, Restaurant restaurant) {
        LOG.info("Updating restaurant: {} to: {}\n", this, restaurant);
        ENTITIES.replace(id, restaurant);
    }

    @Override
    public void delete(Long id) {
        LOG.info("Removing a restaurant\n");
        ENTITIES.remove(id);
    }

    @Override
    public Collection<Restaurant> findAll() {
        LOG.info("Finding all restaurants\n");
        return ENTITIES.values();
    }

    @Override
    public Restaurant findByName(String name) {
        for (Restaurant restaurant : ENTITIES.values()) {
            if (restaurant.getName().equals(name)) {
                return restaurant;
            }
        }
        return null;
    }
}
