package edu.bbte.idde.nzim2062.backend.dao;

import edu.bbte.idde.nzim2062.backend.config.Config;
import edu.bbte.idde.nzim2062.backend.config.ConfigFactory;
import edu.bbte.idde.nzim2062.backend.dao.jdbc.JdbcDaoFactory;
import edu.bbte.idde.nzim2062.backend.dao.mem.MemDaoFactory;

public abstract class DaoFactory {
    private static DaoFactory instance;

    public abstract MenuDao getMenuDao();

    public abstract RestaurantDao getRestaurantDao();

    public static synchronized DaoFactory getInstance() {
        if (instance == null) {
            Config config = ConfigFactory.getConfig();

            if ("jdbc".equals(config.getDaoType())) {
                instance = new JdbcDaoFactory();
            } else {
                instance = new MemDaoFactory();
            }
        }
        return instance;
    }
}
