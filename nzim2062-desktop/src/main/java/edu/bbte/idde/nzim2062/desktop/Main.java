package edu.bbte.idde.nzim2062.desktop;

import edu.bbte.idde.nzim2062.backend.dao.DaoFactory;
import edu.bbte.idde.nzim2062.backend.dao.MenuDao;
import edu.bbte.idde.nzim2062.backend.dao.RestaurantDao;
import edu.bbte.idde.nzim2062.backend.model.Menu;
import edu.bbte.idde.nzim2062.backend.model.Restaurant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        DaoFactory daoFactory = DaoFactory.getInstance();

        RestaurantDao restaurantDao = daoFactory.getRestaurantDao();

        Restaurant restaurant1 = new Restaurant(90L, "Szep Csarda");
        Restaurant restaurant2 = new Restaurant(90L, "Szep Csarda2");
        restaurantDao.create(restaurant1);
        restaurantDao.create(restaurant2);

        LOG.info("Vendeglok:");
        for (Restaurant restaurant : restaurantDao.findAll()) {
            LOG.info(restaurant.toString());
        }

        LOG.info("V: {}", restaurantDao.findByName("Ize"));
        LOG.info("V: {}", restaurantDao.findByName("Szep Csarda"));

        MenuDao dao = daoFactory.getMenuDao();

        LOG.info("Find by name:" + dao.findByName("Rantott husos"));
        Menu menuke = new Menu("Ize", 13.0, "fgfggfk", "glgffgn", false, 1L);
        dao.create(menuke);

        LOG.info("Menuk:");
        for (Menu menu : dao.findAll()) {
            LOG.info(menu.toString());
        }

        //dao.update(1L, menuke);

        LOG.info("Menuk:");
        for (Menu menu : dao.findAll()) {
            LOG.info(menu.toString());
        }
    }
}
