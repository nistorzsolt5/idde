create database IF NOT EXISTS Catering;
USE Catering;

create table IF NOT EXISTS Menus(
	menuName VARCHAR(256) NOT NULL,
	price DOUBLE NOT NULL,
	possibleAlergies VARCHAR(256) NOT NULL,
	ingredients VARCHAR(256) NOT NULL,
	vegan BOOL NOT NULL,
	id INT NOT NULL auto_increment PRIMARY KEY,
    
	FK INT,
    FOREIGN KEY (FK) REFERENCES Restaurants(id)
);

create table IF NOT EXISTS Restaurants(
	name VARCHAR(256) NOT NULL,
	id INT NOT NULL auto_increment PRIMARY KEY
);