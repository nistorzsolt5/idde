package edu.bbte.idde.nzim2062.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.bbte.idde.nzim2062.backend.dao.DaoFactory;
import edu.bbte.idde.nzim2062.backend.dao.MenuDao;
import edu.bbte.idde.nzim2062.backend.dao.RestaurantDao;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

@WebServlet("/counter")
public class CounterServlet extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(MenuServlet.class);

    private MenuDao menuDao;
    private ObjectMapper objectMapper;

    @Override
    public void init() throws ServletException {
        LOG.info("Servlet initialized");
        menuDao = DaoFactory.getInstance().getMenuDao();
        objectMapper = ObjectMapperFactory.getObjectMapper();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.info("Get method");

        try {
            objectMapper.writeValue(resp.getOutputStream(),  menuDao.getCounter());

        } catch (
                NumberFormatException e) {
            LOG.warn("A problema: " + e);
            resp.sendError(400);
        }
    }
}