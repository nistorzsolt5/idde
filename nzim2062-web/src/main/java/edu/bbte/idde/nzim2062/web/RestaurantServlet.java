package edu.bbte.idde.nzim2062.web;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.bbte.idde.nzim2062.backend.dao.DaoFactory;
import edu.bbte.idde.nzim2062.backend.dao.RestaurantDao;
import edu.bbte.idde.nzim2062.backend.model.Restaurant;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

@WebServlet("/restaurants")
public class RestaurantServlet extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(MenuServlet.class);

    private RestaurantDao restaurantDao;

    private ObjectMapper objectMapper;

    @Override
    public void init() throws ServletException {
        LOG.info("Servlet initialized");
        restaurantDao = DaoFactory.getInstance().getRestaurantDao();
        objectMapper = ObjectMapperFactory.getObjectMapper();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.info("Get method");

        if (req.getParameterMap().containsKey("id")) {
            try {
                Long id = Long.valueOf(req.getParameter("id"));
                LOG.info("Got the id {}", id);

                if (restaurantDao.findById(id) == null) {
                    resp.sendError(404);
                    return;
                } else {
                    objectMapper.writeValue(resp.getOutputStream(), restaurantDao.findById(id));
                }
            } catch (NumberFormatException e) {
                LOG.warn("A problema: " + e);
                resp.sendError(400);
            }
        } else {
            resp.setHeader("Content-Type", "application/json");
            objectMapper.writeValue(resp.getOutputStream(), restaurantDao.findAll());
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            Restaurant restaurant = objectMapper.readValue(req.getInputStream(), Restaurant.class);
            LOG.info("POST request on: {}", restaurant);

            restaurantDao.create(restaurant);

        } catch (JSONException | JsonMappingException e) {
            LOG.warn("A problema: " + e);
            resp.sendError(400);
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Restaurant restaurant = objectMapper.readValue(req.getInputStream(), Restaurant.class);
            LOG.info("POST request on: {}", restaurant);

            restaurantDao.update(restaurant.getId(), restaurant);

        } catch (JSONException | JsonMappingException e) {
            LOG.warn("A problema: " + e);
            resp.sendError(400);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameterMap().containsKey("id")) {
            try {
                Long id = Long.valueOf(req.getParameter("id"));
                if (restaurantDao.findById(id) == null) {
                    resp.sendError(404);
                } else {
                    LOG.info("DELETED: {}", restaurantDao.findById(id));
                    restaurantDao.delete(id);
                }
            } catch (NumberFormatException e) {
                LOG.warn("A problema: " + e);
                resp.sendError(400);
            }
        } else {
            LOG.warn("Does not contain a ID parameter");
            resp.sendError(400);
        }
    }
}
