package edu.bbte.idde.nzim2062.web;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(LoginServlet.class);

    private Template template;

    @Override
    public void init() throws ServletException {
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_23);
        cfg.setDefaultEncoding("UTF-8");
        cfg.setClassForTemplateLoading(MenuListServlet.class, "/");
        try {
            template = cfg.getTemplate("Login.ftl");
        } catch (IOException e) {
            throw new ServletException();
        }
        LOG.info("LoginServlet Servlet initialized");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession(false);

        if (session == null) {
            try {
                template.process(null, resp.getWriter());
            } catch (TemplateException e) {
                throw new ServletException();
            }
        } else {
            LOG.info("Redirecting to menu-list");
            resp.sendRedirect("/menu-list");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        String name = request.getParameter("name");
        String password = request.getParameter("password");

        if ("admin".equals(name) && "admin".equals(password)) {
            LOG.info("SUCCESFUL LOGIN " + name);
            request.getSession();
            response.sendRedirect("menu-list");
        } else {
            LOG.info("UNSUCCESFUL LOGIN " + name + password);
            response.sendError(401, "Sorry, username or password error!");
        }
        out.close();
    }
}
