package edu.bbte.idde.nzim2062.web;

import edu.bbte.idde.nzim2062.backend.dao.DaoFactory;
import edu.bbte.idde.nzim2062.backend.dao.MenuDao;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@WebServlet("/menu-list")
public class MenuListServlet extends HttpServlet {

    private MenuDao menuDao;

    private Template template;

    @Override
    public void init() throws ServletException {
        menuDao = DaoFactory.getInstance().getMenuDao();

        Configuration cfg = new Configuration(Configuration.VERSION_2_3_23);
        cfg.setDefaultEncoding("UTF-8");
        cfg.setClassForTemplateLoading(MenuListServlet.class, "/");
        try {
            template = cfg.getTemplate("MenuList.ftl");
        } catch (IOException e) {
            throw new ServletException();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Map<String, Object> templateData = new ConcurrentHashMap<>();
            templateData.put("menus", menuDao.findAll());

            template.process(templateData, resp.getWriter());
        } catch (TemplateException e) {
            throw new ServletException();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getSession().invalidate();
        resp.sendRedirect("login");
    }
}
