<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Menuk</title>
    <#include "style.css">
</head>
<body>
    <h1>Menuk listaja</h1>

    <form method="POST">
        <input type="SUBMIT" value="logout">
    </form>

    <#list menus as menu>
        <p>${menu.id}: ${menu.menuname}</p>

        <p>Osszetevok: ${menu.ingredients}</p>

        <#if menu.vegan>
            <p>A menu vegan</p>
        <#else>
            <p>A menu nem vegan</p>
        </#if>

        <p>Allergia: ${menu.possiblealergies}</p>

        <br> <br>
    </#list>
</body>
</html>
