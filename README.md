Web application that manages Restaurants and Menus, with signup and login functions. Key technologies and components used in the project include:

Frontend: Built using HTML and CSS for a responsive and accessible user interface.
Backend: Implemented with the Spring Boot framework using Java, providing robust and scalable server-side logic.
Database: Utilized MySQL for data storage, managed through Spring Boot JPA (Java Persistence API) for efficient database operations.
