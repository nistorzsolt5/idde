package edu.bbte.idde.nzim2062.spring.dto.outgoing;

import lombok.Data;

@Data
public class MenuOutDto {
    Long id;
    RestaurantOutDto restaurant;
    String name;
    Double price;
    String ingredients;
    String possiblealergies;
    Boolean vegan;
}
