package edu.bbte.idde.nzim2062.spring.dao.jpa;

import edu.bbte.idde.nzim2062.spring.dao.RestaurantDao;
import edu.bbte.idde.nzim2062.spring.model.Restaurant;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Profile("jpa")
public interface JpaRestaurantDao extends RestaurantDao, JpaRepository<Restaurant, Long> {
}
