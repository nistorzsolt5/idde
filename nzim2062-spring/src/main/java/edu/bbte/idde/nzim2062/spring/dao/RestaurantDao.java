package edu.bbte.idde.nzim2062.spring.dao;


import edu.bbte.idde.nzim2062.spring.model.Restaurant;


import java.util.Collection;

public interface RestaurantDao extends Dao<Restaurant> {
    Collection<Restaurant> findByName(String name);
}
