package edu.bbte.idde.nzim2062.spring.controller;

import edu.bbte.idde.nzim2062.spring.controller.exception.MethodArgumentTypeMismatchException;
import edu.bbte.idde.nzim2062.spring.controller.exception.PreconditionFailedException;
import edu.bbte.idde.nzim2062.spring.dao.MenuDao;
import edu.bbte.idde.nzim2062.spring.dao.RestaurantDao;
import edu.bbte.idde.nzim2062.spring.controller.exception.NotFoundException;
import edu.bbte.idde.nzim2062.spring.model.Menu;
import edu.bbte.idde.nzim2062.spring.mapper.MenuMapper;
import edu.bbte.idde.nzim2062.spring.dto.incoming.MenuInDto;
import edu.bbte.idde.nzim2062.spring.dto.outgoing.MenuOutDto;
import edu.bbte.idde.nzim2062.spring.model.Restaurant;
import lombok.extern.slf4j.Slf4j;
import net.bytebuddy.implementation.bytecode.Throw;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.time.Instant;
import java.util.Collection;

@Slf4j
@RestController
@RequestMapping("/menus")
public class MenuController {
    @Autowired
    private MenuDao menuDao;
    @Autowired
    private RestaurantDao restaurantDao;
    @Autowired
    private MenuMapper menuMapper;

    @GetMapping
    public Collection<MenuOutDto> findAll(@RequestParam(required = false) String name) {
        if (name == null) {
            return menuMapper.dtosFromMenus(menuDao.findAll());
        } else {
            return menuMapper.dtosFromMenus(menuDao.findByName(name));
        }
    }

    @PostMapping
    public MenuOutDto create(@RequestBody @Valid MenuInDto menuInDto) {
        try {
            Restaurant restaurant = restaurantDao.getById(menuInDto.getRestaurantID());
            Menu menu = menuMapper.menuFromDto(menuInDto);

            if (restaurant == null) {
                throw new NotFoundException("Nem sikerult insertelni");
            } else {
                menu.setRestaurant(restaurant);
                return menuMapper.dtoFromMenu(menuDao.saveAndFlush(menu));
            }
        } catch (EntityNotFoundException e) {
            throw new NotFoundException("Nem letezik a Vendeglo, amihez be akarok szurni");
        }
    }

    @PutMapping("/{id}")
    public void update(@RequestBody @Valid MenuInDto menuInDto, @PathVariable("id") Long id) {

        try {
            Menu menu = menuMapper.menuFromDto(menuInDto);
            Restaurant restaurant = restaurantDao.getById(menuInDto.getRestaurantID());
            menu.setRestaurant(restaurant);
            menu.setId(id);

            if (restaurant == null || menuDao.getById(id) == null) {
                throw new EntityNotFoundException();
            } else {
                menuDao.saveAndFlush(menu);
            }
        } catch (EntityNotFoundException e) {
            throw new NotFoundException("Nem letezik a Vendeglo, amihez be akarok szurni");
        }
    }

    @GetMapping("/{id}")
    public MenuOutDto findById(@PathVariable("id") Long id, @RequestParam(required = false) String when) {
        try {
            Menu result = menuDao.getById(id);
            if (result == null) {
                throw new EntityNotFoundException();
            } else {

                if (when == null) {
                    throw new PreconditionFailedException("Parameter not found: when");
                } else if (when.equals("on")) {
                    result.setTime(Instant.now());
                    return menuMapper.detailedDtosFromMenus(result);
                } else if (when.equals("off")) {
                    return menuMapper.dtoFromMenu(result);
                } else {
                    throw new MethodArgumentTypeMismatchException("Value not supported " + when);
                }
            }
        } catch (EntityNotFoundException e) {
            throw new NotFoundException("Nem letezik a Menu");
        }
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") Long id) {
        try {
            Menu result = menuDao.getById(id);
            if (result == null) {
                throw new EntityNotFoundException();
            }
            if (id == null) {
                throw new NotFoundException("Nincs megadva id");
            } else {
                menuDao.deleteById(id);
            }
        } catch (EntityNotFoundException e) {
            throw new NotFoundException("Nem letezik a Menu, amit kell torolni");
        }

    }
}
