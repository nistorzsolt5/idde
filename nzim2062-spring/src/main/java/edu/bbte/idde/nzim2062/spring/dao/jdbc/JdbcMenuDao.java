package edu.bbte.idde.nzim2062.spring.dao.jdbc;

import edu.bbte.idde.nzim2062.spring.dao.MenuDao;
import edu.bbte.idde.nzim2062.spring.dao.exception.DaoRunTimeException;
import edu.bbte.idde.nzim2062.spring.model.Menu;
import edu.bbte.idde.nzim2062.spring.model.Restaurant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

@Repository
@Profile("jdbc")
public class JdbcMenuDao implements MenuDao {
    @Autowired
    private DataSource dataSource;

    @Autowired
    private JdbcRestaurantDao restaurantDao;

    private static final Logger LOG = LoggerFactory.getLogger(JdbcMenuDao.class);

    private Collection<Menu> resultSetCollection(ResultSet set)
            throws SQLException {
        Collection<Menu> menus = new ArrayList<>();

        while (set.next()) {
            Menu menu = new Menu();
            Restaurant restaurant = restaurantDao.getById(set.getLong("restaurants_id"));

            menu.setId(set.getLong("id"));
            menu.setName(set.getString("name"));
            menu.setPrice(set.getDouble("price"));
            menu.setPossiblealergies(set.getString("possibleAlergies"));
            menu.setIngredients(set.getString("ingredients"));
            menu.setVegan(set.getBoolean("vegan"));
            menu.setRestaurant(restaurant);

            menus.add(menu);
        }

        return menus;
    }

    private Menu resultSet(ResultSet set) throws SQLException {
        if (set.next()) {
            Menu menu = new Menu();
            Restaurant restaurant = restaurantDao.getById(set.getLong("restaurants_id"));

            menu.setId(set.getLong("id"));
            menu.setName(set.getString("name"));
            menu.setPrice(set.getDouble("price"));
            menu.setPossiblealergies(set.getString("possiblealergies"));
            menu.setIngredients(set.getString("ingredients"));
            menu.setVegan(set.getBoolean("vegan"));
            menu.setRestaurant(restaurant);

            return menu;
        }
        return null;
    }

    private void prepareStatementMenu(Menu menu, PreparedStatement prep) throws SQLException {
        prep.setString(1, menu.getName());
        prep.setDouble(2, menu.getPrice());
        prep.setString(3, menu.getPossiblealergies());
        prep.setString(4, menu.getIngredients());
        prep.setBoolean(5, menu.getVegan());
        Restaurant restaurant = menu.getRestaurant();
        prep.setLong(6, restaurant.getId());
    }

    @Override
    public Menu saveAndFlush(Menu menu) {
        LOG.info("Creating a new menu\n");

        try (Connection connection = dataSource.getConnection()) {

            if (menu.getId() == null) {

                PreparedStatement prep = connection
                        .prepareStatement("""
                                insert into Menus(name, price, possiblealergies, ingredients, vegan, restaurants_id, id)
                                values (?, ?, ?, ?, ?, ?, default)
                                """, new String[]{"id"});
                prepareStatementMenu(menu, prep);
                prep.executeUpdate();

                ResultSet keys = prep.getGeneratedKeys();
                if (keys.next()) {
                    menu.setId(keys.getLong(1));
                }

            } else {
                Long id = menu.getId();
                LOG.info("Updating menu: {} to: {}\n", getById(id), menu);

                PreparedStatement prep = connection
                        .prepareStatement("""
                                update Menus
                                set name = ?,
                                price = ?,
                                possibleAlergies = ?,
                                ingredients = ?,
                                vegan = ?,
                                restaurants_id = ?
                                where id = ?;""");
                prepareStatementMenu(menu, prep);
                prep.setLong(7, id);
                prep.executeUpdate();
            }

            return menu;

        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
            throw new DaoRunTimeException("SQL HIBA");
        }
    }

    @Override
    public void deleteById(Long id) {
        LOG.info("Removing a menu\n");

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("delete from Menus where id = ?;");
            prep.setLong(1, id);
            prep.executeUpdate();
        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
            throw new DaoRunTimeException("SQL HIBA");
        }
    }

    @Override
    public Menu getById(Long id) {
        LOG.info("Finding a menu by the ID: {}\n", id);

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("select * from Menus where id = ?;");
            prep.setLong(1, id);
            ResultSet set = prep.executeQuery();
            return resultSet(set);

        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
            throw new DaoRunTimeException("SQL HIBA");
        }
    }

    @Override
    public Collection<Menu> findAll() {
        LOG.info("Finding all menus\n");

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("select * from Menus");
            ResultSet set = prep.executeQuery();
            return resultSetCollection(set);

        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
            throw new DaoRunTimeException("SQL HIBA");
        }
    }

    @Override
    public Collection<Menu> findByName(String name) {
        LOG.info("Finding a menu by the ID\n");

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("select * from Menus where name = ?;");
            prep.setString(1, name);
            ResultSet set = prep.executeQuery();
            return resultSetCollection(set);

        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
            throw new DaoRunTimeException("SQL HIBA");
        }
    }
}