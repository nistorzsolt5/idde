package edu.bbte.idde.nzim2062.spring.dao.jdbc;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import javax.sql.DataSource;

@Configuration
@Profile("jdbc")

public class DataSourceFactory {

    @Value("com.mysql.cj.jdbc.Driver")
    private  String driverClass;
    @Value("${jdbc.url:localhost-0-0}")
    private  String jdbcUrl;
    @Value("${jdbc.user:root}")
    private  String jdbcUser;
    @Value("${jdbc.passwd:root}")
    private  String passwd;
    @Value("${jdbc.poolSize:10}")
    private  Integer connectionNumber;

    @Bean
    // @Scope("Prototype")
    public DataSource getDataSource() {
        HikariConfig hikariConfig = new HikariConfig();

        hikariConfig.setDriverClassName(driverClass);
        hikariConfig.setJdbcUrl(jdbcUrl);
        hikariConfig.setUsername(jdbcUser);
        hikariConfig.setPassword(passwd);
        hikariConfig.setMaximumPoolSize(connectionNumber);

        return new HikariDataSource(hikariConfig);
    }
}
