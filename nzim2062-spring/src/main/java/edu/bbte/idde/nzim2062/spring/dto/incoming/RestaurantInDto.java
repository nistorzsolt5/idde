package edu.bbte.idde.nzim2062.spring.dto.incoming;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
public class RestaurantInDto {
    @NotNull
    @Length(max = 250)
    String name;
}
