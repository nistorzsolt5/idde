package edu.bbte.idde.nzim2062.spring.mapper;

import edu.bbte.idde.nzim2062.spring.dto.incoming.MenuInDto;
import edu.bbte.idde.nzim2062.spring.dto.outgoing.MenuOutDetailedDto;
import edu.bbte.idde.nzim2062.spring.dto.outgoing.MenuOutDto;
import edu.bbte.idde.nzim2062.spring.model.Menu;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;

import java.util.Collection;

@Mapper(componentModel = "spring")
public abstract class MenuMapper {
    public abstract Menu menuFromDto(MenuInDto menuInDto);

    public abstract MenuOutDto dtoFromMenu(Menu menu);

    @IterableMapping(elementTargetType = MenuOutDto.class)
    public abstract Collection<MenuOutDto> dtosFromMenus(Collection<Menu> menus);

    public abstract MenuOutDetailedDto detailedDtosFromMenus(Menu menu);
}