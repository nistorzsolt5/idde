package edu.bbte.idde.nzim2062.spring.dao.mem;

import edu.bbte.idde.nzim2062.spring.dao.MenuDao;
import edu.bbte.idde.nzim2062.spring.model.Menu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@Repository
@Profile("mem")
public class MemMenuDao implements MenuDao {
    private static final Map<Long, Menu> ENTITIES = new ConcurrentHashMap<>();

    private static final AtomicLong ID_GENERATOR = new AtomicLong();

    private static final Logger LOG = LoggerFactory.getLogger(MemMenuDao.class);

    @Override
    public Menu getById(Long id) {
        LOG.info("Finding a menu by the ID\n");
        return ENTITIES.get(id);
    }

    @Override
    public Menu saveAndFlush(Menu menu) {
        if (menu.getId() == null) {
            LOG.info("Creating a new menu\n");
            Long id = ID_GENERATOR.getAndIncrement();
            menu.setId(id);
            ENTITIES.put(id, menu);
        } else {
            LOG.info("Updating menu: {} to: {}\n", this, menu);
            ENTITIES.replace(menu.getId(), menu);
        }
        return menu;
    }

    @Override
    public void deleteById(Long id) {
        LOG.info("Removing a menu\n");
        ENTITIES.remove(id);
    }

    @Override
    public Collection<Menu> findAll() {
        LOG.info("Finding all menus\n");
        return ENTITIES.values();
    }

    @Override
    public Collection<Menu> findByName(String name) {
        LOG.info("Finding a menu by the ID\n");
        Collection<Menu> menus = new ArrayList<>();

        for (Menu menu : ENTITIES.values()) {
            if (menu.getName().equals(name)) {
                menus.add(menu);
            }
        }
        return menus;
    }
}
