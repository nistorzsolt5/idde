package edu.bbte.idde.nzim2062.spring.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;

@Data
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "Restaurants")

public class Restaurant extends BaseEntity {
    @NotNull
    protected String name;

    @ToString.Exclude
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "restaurant", fetch = FetchType.EAGER)
    private Collection<Menu> menuCollection = new ArrayList<>();
}
