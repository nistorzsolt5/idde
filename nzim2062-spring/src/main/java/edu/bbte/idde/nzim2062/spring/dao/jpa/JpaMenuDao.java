package edu.bbte.idde.nzim2062.spring.dao.jpa;

import edu.bbte.idde.nzim2062.spring.dao.MenuDao;
import edu.bbte.idde.nzim2062.spring.model.Menu;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Profile("jpa")
public interface JpaMenuDao extends MenuDao, JpaRepository<Menu, Long> {
}
