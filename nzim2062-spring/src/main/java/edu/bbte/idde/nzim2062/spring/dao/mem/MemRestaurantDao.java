package edu.bbte.idde.nzim2062.spring.dao.mem;

import edu.bbte.idde.nzim2062.spring.dao.RestaurantDao;
import edu.bbte.idde.nzim2062.spring.model.Restaurant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class MemRestaurantDao implements RestaurantDao {
    private static final Logger LOG = LoggerFactory.getLogger(MemRestaurantDao.class);

    private static final AtomicLong ID_GENERATOR = new AtomicLong();
    private static final Map<Long, Restaurant> ENTITIES = new ConcurrentHashMap<>();


    @Override
    public Restaurant saveAndFlush(Restaurant restarant) {
        if (restarant.getId() == null) {
            LOG.info("Creating a new restarant\n");
            Long id = ID_GENERATOR.getAndIncrement();
            restarant.setId(id);
            ENTITIES.put(id, restarant);
        } else {
            LOG.info("Updating restarant: {} to: {}\n", this, restarant);
            ENTITIES.replace(restarant.getId(), restarant);
        }
        return restarant;
    }

    @Override
    public Restaurant getById(Long id) {
        LOG.info("Finding a restaurant by the ID: {}\n", id);
        return ENTITIES.get(id);
    }

    @Override
    public void deleteById(Long id) {
        LOG.info("Removing a restaurant\n");
        ENTITIES.remove(id);
    }

    @Override
    public Collection<Restaurant> findAll() {
        LOG.info("Finding all restaurants\n");
        return ENTITIES.values();
    }

    @Override
    public Collection<Restaurant> findByName(String name) {
        Collection<Restaurant> restaurants = new ArrayList<>();

        for (Restaurant restaurant : ENTITIES.values()) {
            if (restaurant.getName().equals(name)) {
                restaurants.add(restaurant);
            }
        }
        return restaurants;
    }
}
