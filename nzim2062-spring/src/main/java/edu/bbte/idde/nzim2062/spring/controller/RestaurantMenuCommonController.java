package edu.bbte.idde.nzim2062.spring.controller;

import edu.bbte.idde.nzim2062.spring.controller.exception.NotFoundException;
import edu.bbte.idde.nzim2062.spring.dao.RestaurantDao;
import edu.bbte.idde.nzim2062.spring.dto.incoming.MenuInDto;
import edu.bbte.idde.nzim2062.spring.dto.outgoing.MenuOutDto;
import edu.bbte.idde.nzim2062.spring.mapper.MenuMapper;
import edu.bbte.idde.nzim2062.spring.model.Menu;
import edu.bbte.idde.nzim2062.spring.model.Restaurant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.util.Collection;
import java.util.Objects;

@Slf4j
@RestController
@RequestMapping("/restaurants/{id}/menu")
public class RestaurantMenuCommonController {

    @Autowired
    private RestaurantDao restaurantDao;

    @Autowired
    private MenuMapper menuMapper;

    @GetMapping()
    public Collection<MenuOutDto> findMenuByRestaurant(@PathVariable("id") Long id) {
        try {
            Restaurant restaurant = restaurantDao.getById(id);

            return menuMapper.dtosFromMenus(restaurant.getMenuCollection());
        } catch (EntityNotFoundException e) {
            throw new NotFoundException("Nem letezik a Vendeglo, az id alapjan");
        }
    }

    @PostMapping()
    public MenuOutDto create(@PathVariable("id") Long idRestaurant,
                                   @RequestBody @Valid MenuInDto menuInDto) {

        try {
            Restaurant restaurant = restaurantDao.getById(idRestaurant);

            Menu menu = menuMapper.menuFromDto(menuInDto);
            menu.setRestaurant(restaurant);
            restaurant.getMenuCollection().add(menu);
            restaurantDao.saveAndFlush(restaurant);

            Collection<Menu> menuCollection = restaurant.getMenuCollection();
            return menuMapper.dtoFromMenu(menuCollection.stream().reduce((prev, next) -> next).orElse(null));

        } catch (EntityNotFoundException e) {
            throw new NotFoundException("Nem letezik a Vendeglo, az id alapjan");
        }
    }

    @DeleteMapping("/{idMenu}")
    public void deleteMenuByRestaurant(@PathVariable("id") Long idRestaurant, @PathVariable("idMenu") Long idMenu) {
        try {
            Restaurant restaurant = restaurantDao.getById(idRestaurant);
            restaurant.getMenuCollection().removeIf(menu -> Objects.equals(menu.getId(), idMenu));
            restaurantDao.saveAndFlush(restaurant);

        } catch (EntityNotFoundException e) {
            throw new NotFoundException("Nem letezik a Vendeglo, az id alapjan");
        }
    }
}
