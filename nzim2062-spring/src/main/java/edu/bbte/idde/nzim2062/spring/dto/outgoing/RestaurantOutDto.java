package edu.bbte.idde.nzim2062.spring.dto.outgoing;

import lombok.Data;

@Data
public class RestaurantOutDto {
    Long id;
    String name;
}
