package edu.bbte.idde.nzim2062.spring.dao;


import edu.bbte.idde.nzim2062.spring.model.BaseEntity;

import java.util.Collection;

public interface Dao<T extends BaseEntity> {
    T getById(Long id);

    T saveAndFlush(T entity);

    void deleteById(Long id);

    Collection<T> findAll();

}
