package edu.bbte.idde.nzim2062.spring.mapper;

import edu.bbte.idde.nzim2062.spring.dto.incoming.RestaurantInDto;
import edu.bbte.idde.nzim2062.spring.dto.outgoing.RestaurantOutDto;
import edu.bbte.idde.nzim2062.spring.model.Restaurant;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;

import java.util.Collection;

@Mapper(componentModel = "spring")
public abstract class RestaurantMapper {
    public abstract Restaurant restaurantFromDto(RestaurantInDto restaurantInDto);

    public abstract RestaurantOutDto dtoFromRestaurant(Restaurant restaurant);

    @IterableMapping(elementTargetType = RestaurantOutDto.class)
    public abstract Collection<RestaurantOutDto> dtosFromRestaurants(Collection<Restaurant> restaurants);
}