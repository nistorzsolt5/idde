package edu.bbte.idde.nzim2062.spring.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.Instant;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Menus")

public class Menu extends BaseEntity {
    @NotNull
    private String name;
    @Positive
    @NotNull
    private Double price;
    @NotNull
    private String ingredients;
    @NotNull
    private String possiblealergies;
    private Boolean vegan;
    @ManyToOne()
    @JoinColumn(name = "restaurants_id")
    private Restaurant restaurant;

    private Instant time;
}
