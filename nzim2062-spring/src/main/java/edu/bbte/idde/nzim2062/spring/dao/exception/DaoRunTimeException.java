package edu.bbte.idde.nzim2062.spring.dao.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
@Data
@AllArgsConstructor
public class DaoRunTimeException extends RuntimeException {
    private String message;
}
