package edu.bbte.idde.nzim2062.spring.controller;

import edu.bbte.idde.nzim2062.spring.controller.exception.InternalServerException;
import edu.bbte.idde.nzim2062.spring.dao.RestaurantDao;
import edu.bbte.idde.nzim2062.spring.controller.exception.NotFoundException;
import edu.bbte.idde.nzim2062.spring.model.Restaurant;
import edu.bbte.idde.nzim2062.spring.mapper.RestaurantMapper;
import edu.bbte.idde.nzim2062.spring.dto.incoming.RestaurantInDto;
import edu.bbte.idde.nzim2062.spring.dto.outgoing.RestaurantOutDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.util.Collection;

@Slf4j
@RestController
@RequestMapping("/restaurants")
public class RestaurantController {
    @Autowired
    private RestaurantDao restaurantDao;
    @Autowired
    private RestaurantMapper restaurantMapper;

    @GetMapping
    public Collection<RestaurantOutDto> findAll(@RequestParam(required = false) String name) {
        if (name == null) {
            return restaurantMapper.dtosFromRestaurants(restaurantDao.findAll());
        } else {
            return restaurantMapper.dtosFromRestaurants(restaurantDao.findByName(name));
        }
    }

    @PostMapping
    public RestaurantOutDto create(@RequestBody @Valid RestaurantInDto restaurantInDto) {
        Restaurant restaurant = restaurantDao.saveAndFlush(restaurantMapper.restaurantFromDto(restaurantInDto));

        if (restaurant == null) {
            throw new InternalServerException("Nem sikerult insertelni");
        } else {
            return restaurantMapper.dtoFromRestaurant(restaurant);
        }
    }

    @PutMapping("/{id}")
    public void update(@RequestBody @Valid RestaurantInDto restaurantInDto, @PathVariable("id") Long id) {
        try {
            Restaurant restaurant = restaurantMapper.restaurantFromDto(restaurantInDto);
            restaurant.setId(id);

            if (restaurantDao.getById(id) == null) {
                throw new EntityNotFoundException();
            } else {
                restaurantDao.saveAndFlush(restaurant);
            }
        } catch (EntityNotFoundException e) {
            throw new NotFoundException("Nem letezik a Vendeglo, az id alapjan");
        }
    }

    @GetMapping("/{id}")
    public RestaurantOutDto findById(@PathVariable("id") Long id) {
        try {
            Restaurant result = restaurantDao.getById(id);
            if (result == null) {
                throw new EntityNotFoundException();
            } else {
                return restaurantMapper.dtoFromRestaurant(result);
            }
        } catch (EntityNotFoundException e) {
            throw new NotFoundException("Nem letezik a Vendeglo, az id alapjan");
        }
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") Long id) {
        try {
            Restaurant result = restaurantDao.getById(id);
            if (id == null || result == null) {
                throw new EntityNotFoundException();
            } else {
                restaurantDao.deleteById(id);
            }
        } catch (EntityNotFoundException e) {
            throw new NotFoundException("Nem letezik a Vendeglo, az id alapjan");
        }
    }
}
