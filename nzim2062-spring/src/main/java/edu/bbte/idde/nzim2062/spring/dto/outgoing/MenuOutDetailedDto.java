package edu.bbte.idde.nzim2062.spring.dto.outgoing;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.time.Instant;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class MenuOutDetailedDto extends MenuOutDto{
    Instant time;
}
