package edu.bbte.idde.nzim2062.spring.dao.jdbc;

import edu.bbte.idde.nzim2062.spring.dao.RestaurantDao;
import edu.bbte.idde.nzim2062.spring.dao.exception.DaoRunTimeException;
import edu.bbte.idde.nzim2062.spring.model.Restaurant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

@Repository
@Profile("jdbc")
public class JdbcRestaurantDao implements RestaurantDao {
    @Autowired
    private DataSource dataSource;
    private static final Logger LOG = LoggerFactory.getLogger(JdbcRestaurantDao.class);

    private Collection<Restaurant> resultSetCollection(ResultSet set) throws SQLException {
        Collection<Restaurant> restaurants = new ArrayList<>();
        while (set.next()) {
            Restaurant restaurant = new Restaurant();
            restaurant.setId(set.getLong("id"));
            restaurant.setName(set.getString("name"));

            restaurants.add(restaurant);
        }
        return restaurants;
    }

    private Restaurant resultSet(ResultSet set) throws SQLException {
        if (set.next()) {
            Restaurant restaurant = new Restaurant();
            restaurant.setId(set.getLong("id"));
            restaurant.setName(set.getString("name"));

            return restaurant;
        }
        return null;
    }

    @Override
    public Restaurant saveAndFlush(Restaurant restaurant) {
        LOG.info("Creating a new restaurant\n");

        try (Connection connection = dataSource.getConnection()) {
            if (restaurant.getId() == null) {

                PreparedStatement prep = connection
                        .prepareStatement("insert into Restaurants values(default, ?)", new String[]{"id"});
                prep.setString(1, restaurant.getName());
                prep.executeUpdate();

                ResultSet keys = prep.getGeneratedKeys();
                if (keys.next()) {
                    restaurant.setId(keys.getLong(1));
                }
            } else {
                PreparedStatement prep = connection
                        .prepareStatement("""
                            update Restaurants
                            set name = ?
                            where id = ?;""");
                prep.setString(1, restaurant.getName());
                prep.setLong(2, restaurant.getId());
                prep.executeUpdate();
            }

            return restaurant;

        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
            throw new DaoRunTimeException("SQL HIBA");
        }
    }

    @Override
    public void deleteById(Long id) {
        LOG.info("Removing a restaurant\n");

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("delete from Restaurants where id = ?;");
            prep.setLong(1, id);
            prep.executeUpdate();
        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
            throw new DaoRunTimeException("SQL HIBA");
        }
    }

    @Override
    public Collection<Restaurant> findAll() {
        LOG.info("Finding all restaurants\n");

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("select * from Restaurants");
            ResultSet set = prep.executeQuery();
            return resultSetCollection(set);

        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
            throw new DaoRunTimeException("SQL HIBA");
        }
    }

    @Override
    public Restaurant getById(Long id) {
        LOG.info("Finding a restaurant by the ID: {}\n", id);

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("select * from Restaurants where id = ?;");
            prep.setLong(1, id);
            ResultSet set = prep.executeQuery();
            return resultSet(set);
        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
            throw new DaoRunTimeException("SQL HIBA");
        }
    }

    @Override
    public Collection<Restaurant> findByName(String name) {
        LOG.info("Finding a restaurant by name: {}\n", name);

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("select * from Restaurants where name = ?;");
            prep.setString(1, name);
            ResultSet set = prep.executeQuery();
            return resultSetCollection(set);


        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
            throw new DaoRunTimeException("SQL HIBA");
        }
    }
}
