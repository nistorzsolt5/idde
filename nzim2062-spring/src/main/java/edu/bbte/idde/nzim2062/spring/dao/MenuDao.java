package edu.bbte.idde.nzim2062.spring.dao;


import edu.bbte.idde.nzim2062.spring.model.Menu;

import java.util.Collection;

public interface MenuDao extends Dao<Menu> {
    Collection<Menu> findByName(String name);
}
