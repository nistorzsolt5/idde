package edu.bbte.idde.nzim2062.spring.dto.incoming;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
public class MenuInDto {
    @NotNull
    @Length(max = 250)
    String name;
    @Positive
    Double price;
    @NotNull
    @Length(max = 250)
    String ingredients;
    @NotNull
    @Length(max = 250)
    String possiblealergies;
    Boolean vegan;
    @NotNull
    @Positive
    Long restaurantID;
}
